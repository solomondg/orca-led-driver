Orca LED Driver
------

Extremely simple LED driver board for 12v common anode LED strips. Should drive up to 30A or so, limited by trace width. 

It's designed to be used with an external controller (Raspberry Pi, ESP32, etc). 

Uses 3x IRLB8743 N-channel MOSFETs with MCP1416 gate drivers, with an STM8S003 generating PWM.

Intended for connection over SPI, but UART and I2C are broken out too. 

Firmware is still a WIP.

Each input and each output connector are electrically equivalent (everything's connected internally), I'm simply getting around the 15A current rating on the terminal blocks and making it easier to connect multiple strips.
