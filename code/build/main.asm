;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.9.0 #11195 (Linux)
;--------------------------------------------------------
	.module main
	.optsdcc -mstm8
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _opbyte_setup
	.globl _gpio_setup
	.globl _clock_setup
	.globl _DELAY_MS
	.globl _set_rgb
	.globl _tim_setup
	.globl _spi_init
	.globl _nr
	.globl _printi
	.globl _printf
	.globl _uart_read
	.globl _uart_write
	.globl _uart_setup
	.globl __uitoa
	.globl _strlen
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area INITIALIZED
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area DABS (ABS)

; default segment ordering for linker
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area CONST
	.area INITIALIZER
	.area CODE

;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME
__interrupt_vect:
	int s_GSINIT ; reset
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME
	.area GSINIT
	.area GSFINAL
	.area GSINIT
__sdcc_gs_init_startup:
__sdcc_init_data:
; stm8_genXINIT() start
	ldw x, #l_DATA
	jreq	00002$
00001$:
	clr (s_DATA - 1, x)
	decw x
	jrne	00001$
00002$:
	ldw	x, #l_INITIALIZER
	jreq	00004$
00003$:
	ld	a, (s_INITIALIZER - 1, x)
	ld	(s_INITIALIZED - 1, x), a
	decw	x
	jrne	00003$
00004$:
; stm8_genXINIT() end
	.area GSFINAL
	jp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME
	.area HOME
__sdcc_program_startup:
	jp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CODE
;	./inc/uart.h: 26: uart_setup()                                       
;	-----------------------------------------
;	 function uart_setup
;	-----------------------------------------
_uart_setup:
;	./inc/uart.h: 28: UART_BRR2 = BRR2;                                         
	mov	0x5233+0, #0x0b
;	./inc/uart.h: 29: UART_BRR1 = BRR1;                                          
	mov	0x5232+0, #0x08
;	./inc/uart.h: 30: UART_CR2 |= (1 << UART_ENABLE_RX) | (1 << UART_ENABLE_TX); 
	ld	a, 0x5235
	or	a, #0x0c
	ld	0x5235, a
;	./inc/uart.h: 32: }
	ret
;	./inc/uart.h: 35: uart_write(uint8_t data)                 
;	-----------------------------------------
;	 function uart_write
;	-----------------------------------------
_uart_write:
;	./inc/uart.h: 37: UART_DR = data;                      
	ldw	x, #0x5231
	ld	a, (0x03, sp)
	ld	(x), a
;	./inc/uart.h: 38: while (!(UART_SR & (1 << UART_TC))); 
00101$:
	ld	a, 0x5230
	bcp	a, #0x40
	jreq	00101$
;	./inc/uart.h: 39: }                                        
	ret
;	./inc/uart.h: 42: uart_read() {
;	-----------------------------------------
;	 function uart_read
;	-----------------------------------------
_uart_read:
;	./inc/uart.h: 43: while (!(UART_SR & (1 << UART_RXNE)));
00101$:
	ld	a, 0x5230
	bcp	a, #0x20
	jreq	00101$
;	./inc/uart.h: 44: return UART_DR;
	ld	a, 0x5231
;	./inc/uart.h: 45: }
	ret
;	./inc/uart.h: 48: printf(const char *str)
;	-----------------------------------------
;	 function printf
;	-----------------------------------------
_printf:
	sub	sp, #2
;	./inc/uart.h: 50: uint8_t __len = strlen(str);
	ldw	x, (0x05, sp)
	pushw	x
	call	_strlen
	addw	sp, #2
	ld	a, xl
	ld	(0x01, sp), a
;	./inc/uart.h: 51: for (uint8_t __i=0; __i<__len; __i++ )
	clr	(0x02, sp)
00103$:
	ld	a, (0x02, sp)
	cp	a, (0x01, sp)
	jrnc	00105$
;	./inc/uart.h: 52: uart_write(str[__i]);     
	clrw	x
	ld	a, (0x02, sp)
	ld	xl, a
	addw	x, (0x05, sp)
	ld	a, (x)
	push	a
	call	_uart_write
	pop	a
;	./inc/uart.h: 51: for (uint8_t __i=0; __i<__len; __i++ )
	inc	(0x02, sp)
	jra	00103$
00105$:
;	./inc/uart.h: 53: }
	addw	sp, #2
	ret
;	./inc/uart.h: 56: printi(uint16_t i)
;	-----------------------------------------
;	 function printi
;	-----------------------------------------
_printi:
	sub	sp, #16
;	./inc/uart.h: 59: _uitoa(i, buf, 10);
	ldw	x, sp
	incw	x
	ldw	y, x
	pushw	x
	push	#0x0a
	pushw	y
	ldw	y, (0x18, sp)
	pushw	y
	call	__uitoa
	addw	sp, #5
	call	_printf
;	./inc/uart.h: 61: }
	addw	sp, #18
	ret
;	./inc/uart.h: 64: nr()
;	-----------------------------------------
;	 function nr
;	-----------------------------------------
_nr:
;	./inc/uart.h: 66: printf("\r\n");
	push	#<(___str_0 + 0)
	push	#((___str_0 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	./inc/uart.h: 67: }
	ret
;	./inc/spi.h: 18: spi_init()
;	-----------------------------------------
;	 function spi_init
;	-----------------------------------------
_spi_init:
;	./inc/spi.h: 23: SPI_CR1 |= 1 << SPE; // Enable SPI
	bset	20992, #6
;	./inc/spi.h: 24: SPI_CR1 |= 0b011 << BR0; // Set 1MHz mode
	ld	a, 0x5200
	or	a, #0x18
	ld	0x5200, a
;	./inc/spi.h: 25: SPI_CR1 &= ~(0b1<<MSTR); // Slave mode
	bres	20992, #2
;	./inc/spi.h: 26: SPI_CR1 &= ~(0b11); // Idle polarity 0, clock phase first
	ld	a, 0x5200
	and	a, #0xfc
	ld	0x5200, a
;	./inc/spi.h: 27: SPI_CR2 |= (1 << SSM) | (0 << SSI);
	bset	20993, #1
;	./inc/spi.h: 28: SPI_CR2 |= (1 << 2); // Set RXOnly (disable output, makes programming easier)
	bset	20993, #2
;	./inc/spi.h: 29: }
	ret
;	./inc/tim.h: 13: tim_setup()
;	-----------------------------------------
;	 function tim_setup
;	-----------------------------------------
_tim_setup:
;	./inc/tim.h: 17: TIM2_PSCR = 2; // Prescale by 4
	mov	0x530e+0, #0x02
;	./inc/tim.h: 20: TIM2_ARRH = 4095 >> 0x8;
	mov	0x530f+0, #0x0f
;	./inc/tim.h: 21: TIM2_ARRL = 4095 & 0xFF;
	mov	0x5310+0, #0xff
;	./inc/tim.h: 24: TIM2_CCMR1 |= (0b111 << 4) | (0 << 3);
	ld	a, 0x5307
	or	a, #0x70
	ld	0x5307, a
;	./inc/tim.h: 26: TIM2_CCMR1 &= ~(0b11 << 0);
	ld	a, 0x5307
	and	a, #0xfc
	ld	0x5307, a
;	./inc/tim.h: 27: TIM2_CCMR2 |= (0b111 << 4) | (0 << 3);
	ld	a, 0x5308
	or	a, #0x70
	ld	0x5308, a
;	./inc/tim.h: 28: TIM2_CCMR2 &= ~(0b11 << 0);
	ld	a, 0x5308
	and	a, #0xfc
	ld	0x5308, a
;	./inc/tim.h: 29: TIM2_CCMR3 |= (0b111 << 4) | (0 << 3);
	ld	a, 0x5309
	or	a, #0x70
	ld	0x5309, a
;	./inc/tim.h: 30: TIM2_CCMR3 &= ~(0b11 << 0);
	ld	a, 0x5309
	and	a, #0xfc
	ld	0x5309, a
;	./inc/tim.h: 33: TIM2_CCER1 |= (0b11 << 4) | (0b11 << 0);
	ld	a, 0x530a
	or	a, #0x33
	ld	0x530a, a
;	./inc/tim.h: 34: TIM2_CCER2 |= (0b11 << 0);
	ld	a, 0x530b
	or	a, #0x03
	ld	0x530b, a
;	./inc/tim.h: 36: TIM2_CR1 |= 0b1;
	bset	21248, #0
;	./inc/tim.h: 38: TIM2_IER |= 0;
	ld	a, 0x5303
	ld	0x5303, a
;	./inc/tim.h: 39: TIM2_IER |= 1 << 6;
	bset	21251, #6
;	./inc/tim.h: 42: }
	ret
;	./inc/tim.h: 67: adjust_vision(uint16_t v)
;	-----------------------------------------
;	 function adjust_vision
;	-----------------------------------------
_adjust_vision:
;	./inc/tim.h: 69: return (uint16_t) ((((uint32_t) v) * ((uint32_t) v)) >> 12);
	ldw	x, (0x03, sp)
	pushw	x
	pushw	x
	call	___muluint2ulong
	addw	sp, #4
	ld	a, #0x0c
00103$:
	srlw	y
	rrcw	x
	dec	a
	jrne	00103$
;	./inc/tim.h: 70: }
	ret
;	./inc/tim.h: 74: set_rgb(uint16_t r, uint16_t g, uint16_t b)
;	-----------------------------------------
;	 function set_rgb
;	-----------------------------------------
_set_rgb:
	sub	sp, #2
;	./inc/tim.h: 76: TIM2_CCR1H = (r) >> 0x8;
	ld	a, (0x05, sp)
	ld	(0x02, sp), a
	clr	(0x01, sp)
	ld	a, (0x02, sp)
	ld	0x5311, a
;	./inc/tim.h: 77: TIM2_CCR1L = (r) & 0xFF;
	ld	a, (0x06, sp)
	ld	0x5312, a
;	./inc/tim.h: 78: TIM2_CCR2H = (g) >> 0x8;
	ld	a, (0x07, sp)
	ld	(0x02, sp), a
	clr	(0x01, sp)
	ld	a, (0x02, sp)
	ld	0x5313, a
;	./inc/tim.h: 79: TIM2_CCR2L = (g) & 0xFF;
	ld	a, (0x08, sp)
	ld	0x5314, a
;	./inc/tim.h: 80: TIM2_CCR3H = (b) >> 0x8;
	ld	a, (0x09, sp)
	ld	(0x02, sp), a
	clr	(0x01, sp)
	ld	a, (0x02, sp)
	ld	0x5315, a
;	./inc/tim.h: 81: TIM2_CCR3L = (b) & 0xFF;
	ld	a, (0x0a, sp)
	ld	0x5316, a
;	./inc/tim.h: 82: }
	addw	sp, #2
	ret
;	./inc/tim.h: 85: get_counter()
;	-----------------------------------------
;	 function get_counter
;	-----------------------------------------
_get_counter:
	sub	sp, #4
;	./inc/tim.h: 87: return (((uint16_t) TIM2_CNTRH) << 8) | ((uint16_t) TIM2_CNTRL);
	ld	a, 0x530c
	ld	xh, a
	clr	(0x02, sp)
	ld	a, 0x530d
	ld	(0x04, sp), a
	clr	(0x03, sp)
	ld	a, (0x02, sp)
	or	a, (0x04, sp)
	rlwa	x
	or	a, (0x03, sp)
	ld	xh, a
;	./inc/tim.h: 88: }
	addw	sp, #4
	ret
;	src/main.c: 20: DELAY_MS(uint16_t __ms)
;	-----------------------------------------
;	 function DELAY_MS
;	-----------------------------------------
_DELAY_MS:
	sub	sp, #10
;	src/main.c: 22: while (__ms--)
	ldw	y, (0x0d, sp)
	ldw	(0x09, sp), y
00104$:
	ldw	y, (0x09, sp)
	ldw	(0x07, sp), y
	ldw	x, (0x09, sp)
	decw	x
	ldw	(0x09, sp), x
	ldw	x, (0x07, sp)
	jreq	00107$
;	src/main.c: 25: __delay_cycles( __t_count(1000));
	nop
	nop
	ldw	x, #0x026e
	ldw	(0x03, sp), x
	clrw	x
	ldw	(0x01, sp), x
00101$:
	ldw	x, (0x03, sp)
	subw	x, #0x0001
	ldw	(0x07, sp), x
	ld	a, (0x02, sp)
	sbc	a, #0x00
	ld	(0x06, sp), a
	ld	a, (0x01, sp)
	sbc	a, #0x00
	ld	(0x05, sp), a
	ldw	y, (0x07, sp)
	ldw	(0x03, sp), y
	ldw	y, (0x05, sp)
	ldw	(0x01, sp), y
	ldw	x, (0x07, sp)
	jrne	00101$
	ldw	x, (0x05, sp)
	jrne	00101$
	nop
	jra	00104$
00107$:
;	src/main.c: 27: }
	addw	sp, #10
	ret
;	src/main.c: 30: clock_setup()
;	-----------------------------------------
;	 function clock_setup
;	-----------------------------------------
_clock_setup:
;	src/main.c: 32: CLK_CSSR |= (1 << 0) | (1 << 2);
	ld	a, 0x50c8
	or	a, #0x05
	ld	0x50c8, a
;	src/main.c: 34: CLK_ECKR |= (1 << 0); // enable HSE clock
	bset	20673, #0
;	src/main.c: 35: CLK_SWR = 0xB4;
	mov	0x50c4+0, #0xb4
;	src/main.c: 37: CLK_CKDIVR = 0x00;
	mov	0x50c6+0, #0x00
;	src/main.c: 40: CLK_PCKENR1 |= (1 << 2) | (1 << 3) | (1 << 5) | (1 << 7) | (1 << 1); 
	ld	a, 0x50c7
	or	a, #0xae
	ld	0x50c7, a
;	src/main.c: 41: }
	ret
;	src/main.c: 44: gpio_setup()
;	-----------------------------------------
;	 function gpio_setup
;	-----------------------------------------
_gpio_setup:
;	src/main.c: 46: PA_DDR |= (1 << STATUS_LED_PIN); // Config open drain
	bset	20482, #3
;	src/main.c: 47: PA_CR1 &= ~(1 << STATUS_LED_PIN); // Set open drain mode
	bres	20483, #3
;	src/main.c: 48: PA_CR2 |= (0 << STATUS_LED_PIN); // Disable fast mode
	ld	a, 0x5004
	ld	0x5004, a
;	src/main.c: 51: PD_DDR |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Enable output
	ld	a, 0x5011
	or	a, #0x1c
	ld	0x5011, a
;	src/main.c: 52: PD_CR1 |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Set push-pull mode
	ld	a, 0x5012
	or	a, #0x1c
	ld	0x5012, a
;	src/main.c: 53: PD_CR2 |= (0 << RED_CHAN) | (0 << GREEN_CHAN) | (0 << BLUE_CHAN); // Set fast mode
	ld	a, 0x5013
	ld	0x5013, a
;	src/main.c: 57: }
	ret
;	src/main.c: 60: opbyte_setup()
;	-----------------------------------------
;	 function opbyte_setup
;	-----------------------------------------
_opbyte_setup:
;	src/main.c: 69: if ((OPT2 == opt2) && (NOPT2 == ~opt2))
	ld	a, 0x4803
	cp	a, #0x02
	jrne	00102$
	ld	a, 0x4804
	clrw	x
	ld	xl, a
	cpw	x, #0xfffd
	jrne	00102$
;	src/main.c: 71: printf("skipping\r\n");
	push	#<(___str_1 + 0)
	push	#((___str_1 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 72: return;
	ret
00102$:
;	src/main.c: 74: printf("Setting DUKR keys....\n\r");
	push	#<(___str_2 + 0)
	push	#((___str_2 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 75: FLASH_DUKR = FLASH_DUKR_KEY1;
	mov	0x5064+0, #0xae
;	src/main.c: 76: FLASH_DUKR = FLASH_DUKR_KEY2;
	mov	0x5064+0, #0x56
;	src/main.c: 77: printf("Waiting for writes....\n\r");
	push	#<(___str_3 + 0)
	push	#((___str_3 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 78: while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_DUL)));
00104$:
	ld	a, 0x505f
	bcp	a, #0x08
	jreq	00104$
;	src/main.c: 80: printf("Unlocking flash....\n\r");
	push	#<(___str_4 + 0)
	push	#((___str_4 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 81: FLASH_CR2 |= (1 << FLASH_CR2_OPT);
	bset	20571, #7
;	src/main.c: 82: FLASH_NCR2 &= ~(1 << FLASH_NCR2_NOPT);
	bres	20572, #7
;	src/main.c: 88: printf("Remapping pins....\n\r");
	push	#<(___str_5 + 0)
	push	#((___str_5 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 89: OPT2 = opt2; // Remap timer 3 from PA3 to PD2
	mov	0x4803+0, #0x02
;	src/main.c: 90: NOPT2 = ~opt2; // Remap timer 3 from PA3 to PD2
	mov	0x4804+0, #0xfd
;	src/main.c: 91: printf("Waiting for writes....\n\r");
	push	#<(___str_3 + 0)
	push	#((___str_3 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 92: while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_EOP)));
00107$:
	ld	a, 0x505f
	bcp	a, #0x04
	jreq	00107$
;	src/main.c: 96: printf("Locking flash....\n\r");
	push	#<(___str_6 + 0)
	push	#((___str_6 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 97: FLASH_IAPSR &= ~(1 << FLASH_IAPSR_DUL);
	bres	20575, #3
;	src/main.c: 98: }
	ret
;	src/main.c: 101: main() 
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	sub	sp, #31
;	src/main.c: 103: clock_setup();
	call	_clock_setup
;	src/main.c: 104: gpio_setup();
	call	_gpio_setup
;	src/main.c: 105: uart_setup();
	call	_uart_setup
;	src/main.c: 107: printf("\033[2J");
	push	#<(___str_7 + 0)
	push	#((___str_7 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 108: printf("\033[1;1H");
	push	#<(___str_8 + 0)
	push	#((___str_8 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 109: printf("Setting opbytes....\n\r");
	push	#<(___str_9 + 0)
	push	#((___str_9 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 110: opbyte_setup();
	call	_opbyte_setup
;	src/main.c: 111: printf("Done!\n\r");
	push	#<(___str_10 + 0)
	push	#((___str_10 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 115: PD_ODR |= ((1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN));
	ld	a, 0x500f
	or	a, #0x1c
	ld	0x500f, a
;	src/main.c: 118: tim_setup();
	call	_tim_setup
;	src/main.c: 119: set_rgb(1, 1, 1);
	push	#0x01
	push	#0x00
	push	#0x01
	push	#0x00
	push	#0x01
	push	#0x00
	call	_set_rgb
	addw	sp, #6
;	src/main.c: 121: printf("\n\rStarting main loop\n\r");
	push	#<(___str_11 + 0)
	push	#((___str_11 + 0) >> 8)
	call	_printf
	addw	sp, #2
;	src/main.c: 140: for (ptr=0; ptr<16; ptr++)
00112$:
	clr	a
00106$:
;	src/main.c: 142: uart_buf[ptr] == uart_read();
	push	a
	call	_uart_read
	pop	a
;	src/main.c: 140: for (ptr=0; ptr<16; ptr++)
	inc	a
	cp	a, #0x10
	jrc	00106$
;	src/main.c: 155: for (ptr=0; ptr<16; ptr++)
	ldw	x, sp
	incw	x
	ldw	(0x1d, sp), x
	clr	(0x1f, sp)
00108$:
;	src/main.c: 157: uart_write(uart_buf[ptr]);
	clrw	x
	ld	a, (0x1f, sp)
	ld	xl, a
	addw	x, (0x1d, sp)
	ld	a, (x)
	push	a
	call	_uart_write
	pop	a
;	src/main.c: 158: nr();
	call	_nr
;	src/main.c: 155: for (ptr=0; ptr<16; ptr++)
	inc	(0x1f, sp)
	ld	a, (0x1f, sp)
	cp	a, #0x10
	jrc	00108$
	jra	00112$
;	src/main.c: 208: }
	addw	sp, #31
	ret
	.area CODE
	.area CONST
	.area CONST
___str_0:
	.db 0x0d
	.db 0x0a
	.db 0x00
	.area CODE
	.area CONST
___str_1:
	.ascii "skipping"
	.db 0x0d
	.db 0x0a
	.db 0x00
	.area CODE
	.area CONST
___str_2:
	.ascii "Setting DUKR keys...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_3:
	.ascii "Waiting for writes...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_4:
	.ascii "Unlocking flash...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_5:
	.ascii "Remapping pins...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_6:
	.ascii "Locking flash...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_7:
	.db 0x1b
	.ascii "[2J"
	.db 0x00
	.area CODE
	.area CONST
___str_8:
	.db 0x1b
	.ascii "[1;1H"
	.db 0x00
	.area CODE
	.area CONST
___str_9:
	.ascii "Setting opbytes...."
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_10:
	.ascii "Done!"
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area CONST
___str_11:
	.db 0x0a
	.db 0x0d
	.ascii "Starting main loop"
	.db 0x0a
	.db 0x0d
	.db 0x00
	.area CODE
	.area INITIALIZER
	.area CABS (ABS)
