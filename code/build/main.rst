                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 3.9.0 #11195 (Linux)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mstm8
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _opbyte_setup
                                     13 	.globl _gpio_setup
                                     14 	.globl _clock_setup
                                     15 	.globl _DELAY_MS
                                     16 	.globl _set_rgb
                                     17 	.globl _tim_setup
                                     18 	.globl _spi_init
                                     19 	.globl _nr
                                     20 	.globl _printi
                                     21 	.globl _printf
                                     22 	.globl _uart_read
                                     23 	.globl _uart_write
                                     24 	.globl _uart_setup
                                     25 	.globl __uitoa
                                     26 	.globl _strlen
                                     27 ;--------------------------------------------------------
                                     28 ; ram data
                                     29 ;--------------------------------------------------------
                                     30 	.area DATA
                                     31 ;--------------------------------------------------------
                                     32 ; ram data
                                     33 ;--------------------------------------------------------
                                     34 	.area INITIALIZED
                                     35 ;--------------------------------------------------------
                                     36 ; Stack segment in internal ram 
                                     37 ;--------------------------------------------------------
                                     38 	.area	SSEG
      FFFFFF                         39 __start__stack:
      FFFFFF                         40 	.ds	1
                                     41 
                                     42 ;--------------------------------------------------------
                                     43 ; absolute external ram data
                                     44 ;--------------------------------------------------------
                                     45 	.area DABS (ABS)
                                     46 
                                     47 ; default segment ordering for linker
                                     48 	.area HOME
                                     49 	.area GSINIT
                                     50 	.area GSFINAL
                                     51 	.area CONST
                                     52 	.area INITIALIZER
                                     53 	.area CODE
                                     54 
                                     55 ;--------------------------------------------------------
                                     56 ; interrupt vector 
                                     57 ;--------------------------------------------------------
                                     58 	.area HOME
      008000                         59 __interrupt_vect:
      008000 82 00 80 07             60 	int s_GSINIT ; reset
                                     61 ;--------------------------------------------------------
                                     62 ; global & static initialisations
                                     63 ;--------------------------------------------------------
                                     64 	.area HOME
                                     65 	.area GSINIT
                                     66 	.area GSFINAL
                                     67 	.area GSINIT
      008007                         68 __sdcc_gs_init_startup:
      008007                         69 __sdcc_init_data:
                                     70 ; stm8_genXINIT() start
      008007 AE 00 00         [ 2]   71 	ldw x, #l_DATA
      00800A 27 07            [ 1]   72 	jreq	00002$
      00800C                         73 00001$:
      00800C 72 4F 00 00      [ 1]   74 	clr (s_DATA - 1, x)
      008010 5A               [ 2]   75 	decw x
      008011 26 F9            [ 1]   76 	jrne	00001$
      008013                         77 00002$:
      008013 AE 00 00         [ 2]   78 	ldw	x, #l_INITIALIZER
      008016 27 09            [ 1]   79 	jreq	00004$
      008018                         80 00003$:
      008018 D6 80 E2         [ 1]   81 	ld	a, (s_INITIALIZER - 1, x)
      00801B D7 00 00         [ 1]   82 	ld	(s_INITIALIZED - 1, x), a
      00801E 5A               [ 2]   83 	decw	x
      00801F 26 F7            [ 1]   84 	jrne	00003$
      008021                         85 00004$:
                                     86 ; stm8_genXINIT() end
                                     87 	.area GSFINAL
      008021 CC 80 04         [ 2]   88 	jp	__sdcc_program_startup
                                     89 ;--------------------------------------------------------
                                     90 ; Home
                                     91 ;--------------------------------------------------------
                                     92 	.area HOME
                                     93 	.area HOME
      008004                         94 __sdcc_program_startup:
      008004 CC 83 3B         [ 2]   95 	jp	_main
                                     96 ;	return from main will return to caller
                                     97 ;--------------------------------------------------------
                                     98 ; code
                                     99 ;--------------------------------------------------------
                                    100 	.area CODE
                                    101 ;	./inc/uart.h: 26: uart_setup()                                       
                                    102 ;	-----------------------------------------
                                    103 ;	 function uart_setup
                                    104 ;	-----------------------------------------
      0080E3                        105 _uart_setup:
                                    106 ;	./inc/uart.h: 28: UART_BRR2 = BRR2;                                         
      0080E3 35 0B 52 33      [ 1]  107 	mov	0x5233+0, #0x0b
                                    108 ;	./inc/uart.h: 29: UART_BRR1 = BRR1;                                          
      0080E7 35 08 52 32      [ 1]  109 	mov	0x5232+0, #0x08
                                    110 ;	./inc/uart.h: 30: UART_CR2 |= (1 << UART_ENABLE_RX) | (1 << UART_ENABLE_TX); 
      0080EB C6 52 35         [ 1]  111 	ld	a, 0x5235
      0080EE AA 0C            [ 1]  112 	or	a, #0x0c
      0080F0 C7 52 35         [ 1]  113 	ld	0x5235, a
                                    114 ;	./inc/uart.h: 32: }
      0080F3 81               [ 4]  115 	ret
                                    116 ;	./inc/uart.h: 35: uart_write(uint8_t data)                 
                                    117 ;	-----------------------------------------
                                    118 ;	 function uart_write
                                    119 ;	-----------------------------------------
      0080F4                        120 _uart_write:
                                    121 ;	./inc/uart.h: 37: UART_DR = data;                      
      0080F4 AE 52 31         [ 2]  122 	ldw	x, #0x5231
      0080F7 7B 03            [ 1]  123 	ld	a, (0x03, sp)
      0080F9 F7               [ 1]  124 	ld	(x), a
                                    125 ;	./inc/uart.h: 38: while (!(UART_SR & (1 << UART_TC))); 
      0080FA                        126 00101$:
      0080FA C6 52 30         [ 1]  127 	ld	a, 0x5230
      0080FD A5 40            [ 1]  128 	bcp	a, #0x40
      0080FF 27 F9            [ 1]  129 	jreq	00101$
                                    130 ;	./inc/uart.h: 39: }                                        
      008101 81               [ 4]  131 	ret
                                    132 ;	./inc/uart.h: 42: uart_read() {
                                    133 ;	-----------------------------------------
                                    134 ;	 function uart_read
                                    135 ;	-----------------------------------------
      008102                        136 _uart_read:
                                    137 ;	./inc/uart.h: 43: while (!(UART_SR & (1 << UART_RXNE)));
      008102                        138 00101$:
      008102 C6 52 30         [ 1]  139 	ld	a, 0x5230
      008105 A5 20            [ 1]  140 	bcp	a, #0x20
      008107 27 F9            [ 1]  141 	jreq	00101$
                                    142 ;	./inc/uart.h: 44: return UART_DR;
      008109 C6 52 31         [ 1]  143 	ld	a, 0x5231
                                    144 ;	./inc/uart.h: 45: }
      00810C 81               [ 4]  145 	ret
                                    146 ;	./inc/uart.h: 48: printf(const char *str)
                                    147 ;	-----------------------------------------
                                    148 ;	 function printf
                                    149 ;	-----------------------------------------
      00810D                        150 _printf:
      00810D 52 02            [ 2]  151 	sub	sp, #2
                                    152 ;	./inc/uart.h: 50: uint8_t __len = strlen(str);
      00810F 1E 05            [ 2]  153 	ldw	x, (0x05, sp)
      008111 89               [ 2]  154 	pushw	x
      008112 CD 84 BD         [ 4]  155 	call	_strlen
      008115 5B 02            [ 2]  156 	addw	sp, #2
      008117 9F               [ 1]  157 	ld	a, xl
      008118 6B 01            [ 1]  158 	ld	(0x01, sp), a
                                    159 ;	./inc/uart.h: 51: for (uint8_t __i=0; __i<__len; __i++ )
      00811A 0F 02            [ 1]  160 	clr	(0x02, sp)
      00811C                        161 00103$:
      00811C 7B 02            [ 1]  162 	ld	a, (0x02, sp)
      00811E 11 01            [ 1]  163 	cp	a, (0x01, sp)
      008120 24 11            [ 1]  164 	jrnc	00105$
                                    165 ;	./inc/uart.h: 52: uart_write(str[__i]);     
      008122 5F               [ 1]  166 	clrw	x
      008123 7B 02            [ 1]  167 	ld	a, (0x02, sp)
      008125 97               [ 1]  168 	ld	xl, a
      008126 72 FB 05         [ 2]  169 	addw	x, (0x05, sp)
      008129 F6               [ 1]  170 	ld	a, (x)
      00812A 88               [ 1]  171 	push	a
      00812B CD 80 F4         [ 4]  172 	call	_uart_write
      00812E 84               [ 1]  173 	pop	a
                                    174 ;	./inc/uart.h: 51: for (uint8_t __i=0; __i<__len; __i++ )
      00812F 0C 02            [ 1]  175 	inc	(0x02, sp)
      008131 20 E9            [ 2]  176 	jra	00103$
      008133                        177 00105$:
                                    178 ;	./inc/uart.h: 53: }
      008133 5B 02            [ 2]  179 	addw	sp, #2
      008135 81               [ 4]  180 	ret
                                    181 ;	./inc/uart.h: 56: printi(uint16_t i)
                                    182 ;	-----------------------------------------
                                    183 ;	 function printi
                                    184 ;	-----------------------------------------
      008136                        185 _printi:
      008136 52 10            [ 2]  186 	sub	sp, #16
                                    187 ;	./inc/uart.h: 59: _uitoa(i, buf, 10);
      008138 96               [ 1]  188 	ldw	x, sp
      008139 5C               [ 1]  189 	incw	x
      00813A 90 93            [ 1]  190 	ldw	y, x
      00813C 89               [ 2]  191 	pushw	x
      00813D 4B 0A            [ 1]  192 	push	#0x0a
      00813F 90 89            [ 2]  193 	pushw	y
      008141 16 18            [ 2]  194 	ldw	y, (0x18, sp)
      008143 90 89            [ 2]  195 	pushw	y
      008145 CD 84 18         [ 4]  196 	call	__uitoa
      008148 5B 05            [ 2]  197 	addw	sp, #5
      00814A CD 81 0D         [ 4]  198 	call	_printf
                                    199 ;	./inc/uart.h: 61: }
      00814D 5B 12            [ 2]  200 	addw	sp, #18
      00814F 81               [ 4]  201 	ret
                                    202 ;	./inc/uart.h: 64: nr()
                                    203 ;	-----------------------------------------
                                    204 ;	 function nr
                                    205 ;	-----------------------------------------
      008150                        206 _nr:
                                    207 ;	./inc/uart.h: 66: printf("\r\n");
      008150 4B 24            [ 1]  208 	push	#<(___str_0 + 0)
      008152 4B 80            [ 1]  209 	push	#((___str_0 + 0) >> 8)
      008154 CD 81 0D         [ 4]  210 	call	_printf
      008157 5B 02            [ 2]  211 	addw	sp, #2
                                    212 ;	./inc/uart.h: 67: }
      008159 81               [ 4]  213 	ret
                                    214 ;	./inc/spi.h: 18: spi_init()
                                    215 ;	-----------------------------------------
                                    216 ;	 function spi_init
                                    217 ;	-----------------------------------------
      00815A                        218 _spi_init:
                                    219 ;	./inc/spi.h: 23: SPI_CR1 |= 1 << SPE; // Enable SPI
      00815A 72 1C 52 00      [ 1]  220 	bset	20992, #6
                                    221 ;	./inc/spi.h: 24: SPI_CR1 |= 0b011 << BR0; // Set 1MHz mode
      00815E C6 52 00         [ 1]  222 	ld	a, 0x5200
      008161 AA 18            [ 1]  223 	or	a, #0x18
      008163 C7 52 00         [ 1]  224 	ld	0x5200, a
                                    225 ;	./inc/spi.h: 25: SPI_CR1 &= ~(0b1<<MSTR); // Slave mode
      008166 72 15 52 00      [ 1]  226 	bres	20992, #2
                                    227 ;	./inc/spi.h: 26: SPI_CR1 &= ~(0b11); // Idle polarity 0, clock phase first
      00816A C6 52 00         [ 1]  228 	ld	a, 0x5200
      00816D A4 FC            [ 1]  229 	and	a, #0xfc
      00816F C7 52 00         [ 1]  230 	ld	0x5200, a
                                    231 ;	./inc/spi.h: 27: SPI_CR2 |= (1 << SSM) | (0 << SSI);
      008172 72 12 52 01      [ 1]  232 	bset	20993, #1
                                    233 ;	./inc/spi.h: 28: SPI_CR2 |= (1 << 2); // Set RXOnly (disable output, makes programming easier)
      008176 72 14 52 01      [ 1]  234 	bset	20993, #2
                                    235 ;	./inc/spi.h: 29: }
      00817A 81               [ 4]  236 	ret
                                    237 ;	./inc/tim.h: 13: tim_setup()
                                    238 ;	-----------------------------------------
                                    239 ;	 function tim_setup
                                    240 ;	-----------------------------------------
      00817B                        241 _tim_setup:
                                    242 ;	./inc/tim.h: 17: TIM2_PSCR = 2; // Prescale by 4
      00817B 35 02 53 0E      [ 1]  243 	mov	0x530e+0, #0x02
                                    244 ;	./inc/tim.h: 20: TIM2_ARRH = 4095 >> 0x8;
      00817F 35 0F 53 0F      [ 1]  245 	mov	0x530f+0, #0x0f
                                    246 ;	./inc/tim.h: 21: TIM2_ARRL = 4095 & 0xFF;
      008183 35 FF 53 10      [ 1]  247 	mov	0x5310+0, #0xff
                                    248 ;	./inc/tim.h: 24: TIM2_CCMR1 |= (0b111 << 4) | (0 << 3);
      008187 C6 53 07         [ 1]  249 	ld	a, 0x5307
      00818A AA 70            [ 1]  250 	or	a, #0x70
      00818C C7 53 07         [ 1]  251 	ld	0x5307, a
                                    252 ;	./inc/tim.h: 26: TIM2_CCMR1 &= ~(0b11 << 0);
      00818F C6 53 07         [ 1]  253 	ld	a, 0x5307
      008192 A4 FC            [ 1]  254 	and	a, #0xfc
      008194 C7 53 07         [ 1]  255 	ld	0x5307, a
                                    256 ;	./inc/tim.h: 27: TIM2_CCMR2 |= (0b111 << 4) | (0 << 3);
      008197 C6 53 08         [ 1]  257 	ld	a, 0x5308
      00819A AA 70            [ 1]  258 	or	a, #0x70
      00819C C7 53 08         [ 1]  259 	ld	0x5308, a
                                    260 ;	./inc/tim.h: 28: TIM2_CCMR2 &= ~(0b11 << 0);
      00819F C6 53 08         [ 1]  261 	ld	a, 0x5308
      0081A2 A4 FC            [ 1]  262 	and	a, #0xfc
      0081A4 C7 53 08         [ 1]  263 	ld	0x5308, a
                                    264 ;	./inc/tim.h: 29: TIM2_CCMR3 |= (0b111 << 4) | (0 << 3);
      0081A7 C6 53 09         [ 1]  265 	ld	a, 0x5309
      0081AA AA 70            [ 1]  266 	or	a, #0x70
      0081AC C7 53 09         [ 1]  267 	ld	0x5309, a
                                    268 ;	./inc/tim.h: 30: TIM2_CCMR3 &= ~(0b11 << 0);
      0081AF C6 53 09         [ 1]  269 	ld	a, 0x5309
      0081B2 A4 FC            [ 1]  270 	and	a, #0xfc
      0081B4 C7 53 09         [ 1]  271 	ld	0x5309, a
                                    272 ;	./inc/tim.h: 33: TIM2_CCER1 |= (0b11 << 4) | (0b11 << 0);
      0081B7 C6 53 0A         [ 1]  273 	ld	a, 0x530a
      0081BA AA 33            [ 1]  274 	or	a, #0x33
      0081BC C7 53 0A         [ 1]  275 	ld	0x530a, a
                                    276 ;	./inc/tim.h: 34: TIM2_CCER2 |= (0b11 << 0);
      0081BF C6 53 0B         [ 1]  277 	ld	a, 0x530b
      0081C2 AA 03            [ 1]  278 	or	a, #0x03
      0081C4 C7 53 0B         [ 1]  279 	ld	0x530b, a
                                    280 ;	./inc/tim.h: 36: TIM2_CR1 |= 0b1;
      0081C7 72 10 53 00      [ 1]  281 	bset	21248, #0
                                    282 ;	./inc/tim.h: 38: TIM2_IER |= 0;
      0081CB C6 53 03         [ 1]  283 	ld	a, 0x5303
      0081CE C7 53 03         [ 1]  284 	ld	0x5303, a
                                    285 ;	./inc/tim.h: 39: TIM2_IER |= 1 << 6;
      0081D1 72 1C 53 03      [ 1]  286 	bset	21251, #6
                                    287 ;	./inc/tim.h: 42: }
      0081D5 81               [ 4]  288 	ret
                                    289 ;	./inc/tim.h: 67: adjust_vision(uint16_t v)
                                    290 ;	-----------------------------------------
                                    291 ;	 function adjust_vision
                                    292 ;	-----------------------------------------
      0081D6                        293 _adjust_vision:
                                    294 ;	./inc/tim.h: 69: return (uint16_t) ((((uint32_t) v) * ((uint32_t) v)) >> 12);
      0081D6 1E 03            [ 2]  295 	ldw	x, (0x03, sp)
      0081D8 89               [ 2]  296 	pushw	x
      0081D9 89               [ 2]  297 	pushw	x
      0081DA CD 83 C0         [ 4]  298 	call	___muluint2ulong
      0081DD 5B 04            [ 2]  299 	addw	sp, #4
      0081DF A6 0C            [ 1]  300 	ld	a, #0x0c
      0081E1                        301 00103$:
      0081E1 90 54            [ 2]  302 	srlw	y
      0081E3 56               [ 2]  303 	rrcw	x
      0081E4 4A               [ 1]  304 	dec	a
      0081E5 26 FA            [ 1]  305 	jrne	00103$
                                    306 ;	./inc/tim.h: 70: }
      0081E7 81               [ 4]  307 	ret
                                    308 ;	./inc/tim.h: 74: set_rgb(uint16_t r, uint16_t g, uint16_t b)
                                    309 ;	-----------------------------------------
                                    310 ;	 function set_rgb
                                    311 ;	-----------------------------------------
      0081E8                        312 _set_rgb:
      0081E8 52 02            [ 2]  313 	sub	sp, #2
                                    314 ;	./inc/tim.h: 76: TIM2_CCR1H = (r) >> 0x8;
      0081EA 7B 05            [ 1]  315 	ld	a, (0x05, sp)
      0081EC 6B 02            [ 1]  316 	ld	(0x02, sp), a
      0081EE 0F 01            [ 1]  317 	clr	(0x01, sp)
      0081F0 7B 02            [ 1]  318 	ld	a, (0x02, sp)
      0081F2 C7 53 11         [ 1]  319 	ld	0x5311, a
                                    320 ;	./inc/tim.h: 77: TIM2_CCR1L = (r) & 0xFF;
      0081F5 7B 06            [ 1]  321 	ld	a, (0x06, sp)
      0081F7 C7 53 12         [ 1]  322 	ld	0x5312, a
                                    323 ;	./inc/tim.h: 78: TIM2_CCR2H = (g) >> 0x8;
      0081FA 7B 07            [ 1]  324 	ld	a, (0x07, sp)
      0081FC 6B 02            [ 1]  325 	ld	(0x02, sp), a
      0081FE 0F 01            [ 1]  326 	clr	(0x01, sp)
      008200 7B 02            [ 1]  327 	ld	a, (0x02, sp)
      008202 C7 53 13         [ 1]  328 	ld	0x5313, a
                                    329 ;	./inc/tim.h: 79: TIM2_CCR2L = (g) & 0xFF;
      008205 7B 08            [ 1]  330 	ld	a, (0x08, sp)
      008207 C7 53 14         [ 1]  331 	ld	0x5314, a
                                    332 ;	./inc/tim.h: 80: TIM2_CCR3H = (b) >> 0x8;
      00820A 7B 09            [ 1]  333 	ld	a, (0x09, sp)
      00820C 6B 02            [ 1]  334 	ld	(0x02, sp), a
      00820E 0F 01            [ 1]  335 	clr	(0x01, sp)
      008210 7B 02            [ 1]  336 	ld	a, (0x02, sp)
      008212 C7 53 15         [ 1]  337 	ld	0x5315, a
                                    338 ;	./inc/tim.h: 81: TIM2_CCR3L = (b) & 0xFF;
      008215 7B 0A            [ 1]  339 	ld	a, (0x0a, sp)
      008217 C7 53 16         [ 1]  340 	ld	0x5316, a
                                    341 ;	./inc/tim.h: 82: }
      00821A 5B 02            [ 2]  342 	addw	sp, #2
      00821C 81               [ 4]  343 	ret
                                    344 ;	./inc/tim.h: 85: get_counter()
                                    345 ;	-----------------------------------------
                                    346 ;	 function get_counter
                                    347 ;	-----------------------------------------
      00821D                        348 _get_counter:
      00821D 52 04            [ 2]  349 	sub	sp, #4
                                    350 ;	./inc/tim.h: 87: return (((uint16_t) TIM2_CNTRH) << 8) | ((uint16_t) TIM2_CNTRL);
      00821F C6 53 0C         [ 1]  351 	ld	a, 0x530c
      008222 95               [ 1]  352 	ld	xh, a
      008223 0F 02            [ 1]  353 	clr	(0x02, sp)
      008225 C6 53 0D         [ 1]  354 	ld	a, 0x530d
      008228 6B 04            [ 1]  355 	ld	(0x04, sp), a
      00822A 0F 03            [ 1]  356 	clr	(0x03, sp)
      00822C 7B 02            [ 1]  357 	ld	a, (0x02, sp)
      00822E 1A 04            [ 1]  358 	or	a, (0x04, sp)
      008230 02               [ 1]  359 	rlwa	x
      008231 1A 03            [ 1]  360 	or	a, (0x03, sp)
      008233 95               [ 1]  361 	ld	xh, a
                                    362 ;	./inc/tim.h: 88: }
      008234 5B 04            [ 2]  363 	addw	sp, #4
      008236 81               [ 4]  364 	ret
                                    365 ;	src/main.c: 20: DELAY_MS(uint16_t __ms)
                                    366 ;	-----------------------------------------
                                    367 ;	 function DELAY_MS
                                    368 ;	-----------------------------------------
      008237                        369 _DELAY_MS:
      008237 52 0A            [ 2]  370 	sub	sp, #10
                                    371 ;	src/main.c: 22: while (__ms--)
      008239 16 0D            [ 2]  372 	ldw	y, (0x0d, sp)
      00823B 17 09            [ 2]  373 	ldw	(0x09, sp), y
      00823D                        374 00104$:
      00823D 16 09            [ 2]  375 	ldw	y, (0x09, sp)
      00823F 17 07            [ 2]  376 	ldw	(0x07, sp), y
      008241 1E 09            [ 2]  377 	ldw	x, (0x09, sp)
      008243 5A               [ 2]  378 	decw	x
      008244 1F 09            [ 2]  379 	ldw	(0x09, sp), x
      008246 1E 07            [ 2]  380 	ldw	x, (0x07, sp)
      008248 27 30            [ 1]  381 	jreq	00107$
                                    382 ;	src/main.c: 25: __delay_cycles( __t_count(1000));
      00824A 9D               [ 1]  383 	nop
      00824B 9D               [ 1]  384 	nop
      00824C AE 02 6E         [ 2]  385 	ldw	x, #0x026e
      00824F 1F 03            [ 2]  386 	ldw	(0x03, sp), x
      008251 5F               [ 1]  387 	clrw	x
      008252 1F 01            [ 2]  388 	ldw	(0x01, sp), x
      008254                        389 00101$:
      008254 1E 03            [ 2]  390 	ldw	x, (0x03, sp)
      008256 1D 00 01         [ 2]  391 	subw	x, #0x0001
      008259 1F 07            [ 2]  392 	ldw	(0x07, sp), x
      00825B 7B 02            [ 1]  393 	ld	a, (0x02, sp)
      00825D A2 00            [ 1]  394 	sbc	a, #0x00
      00825F 6B 06            [ 1]  395 	ld	(0x06, sp), a
      008261 7B 01            [ 1]  396 	ld	a, (0x01, sp)
      008263 A2 00            [ 1]  397 	sbc	a, #0x00
      008265 6B 05            [ 1]  398 	ld	(0x05, sp), a
      008267 16 07            [ 2]  399 	ldw	y, (0x07, sp)
      008269 17 03            [ 2]  400 	ldw	(0x03, sp), y
      00826B 16 05            [ 2]  401 	ldw	y, (0x05, sp)
      00826D 17 01            [ 2]  402 	ldw	(0x01, sp), y
      00826F 1E 07            [ 2]  403 	ldw	x, (0x07, sp)
      008271 26 E1            [ 1]  404 	jrne	00101$
      008273 1E 05            [ 2]  405 	ldw	x, (0x05, sp)
      008275 26 DD            [ 1]  406 	jrne	00101$
      008277 9D               [ 1]  407 	nop
      008278 20 C3            [ 2]  408 	jra	00104$
      00827A                        409 00107$:
                                    410 ;	src/main.c: 27: }
      00827A 5B 0A            [ 2]  411 	addw	sp, #10
      00827C 81               [ 4]  412 	ret
                                    413 ;	src/main.c: 30: clock_setup()
                                    414 ;	-----------------------------------------
                                    415 ;	 function clock_setup
                                    416 ;	-----------------------------------------
      00827D                        417 _clock_setup:
                                    418 ;	src/main.c: 32: CLK_CSSR |= (1 << 0) | (1 << 2);
      00827D C6 50 C8         [ 1]  419 	ld	a, 0x50c8
      008280 AA 05            [ 1]  420 	or	a, #0x05
      008282 C7 50 C8         [ 1]  421 	ld	0x50c8, a
                                    422 ;	src/main.c: 34: CLK_ECKR |= (1 << 0); // enable HSE clock
      008285 72 10 50 C1      [ 1]  423 	bset	20673, #0
                                    424 ;	src/main.c: 35: CLK_SWR = 0xB4;
      008289 35 B4 50 C4      [ 1]  425 	mov	0x50c4+0, #0xb4
                                    426 ;	src/main.c: 37: CLK_CKDIVR = 0x00;
      00828D 35 00 50 C6      [ 1]  427 	mov	0x50c6+0, #0x00
                                    428 ;	src/main.c: 40: CLK_PCKENR1 |= (1 << 2) | (1 << 3) | (1 << 5) | (1 << 7) | (1 << 1); 
      008291 C6 50 C7         [ 1]  429 	ld	a, 0x50c7
      008294 AA AE            [ 1]  430 	or	a, #0xae
      008296 C7 50 C7         [ 1]  431 	ld	0x50c7, a
                                    432 ;	src/main.c: 41: }
      008299 81               [ 4]  433 	ret
                                    434 ;	src/main.c: 44: gpio_setup()
                                    435 ;	-----------------------------------------
                                    436 ;	 function gpio_setup
                                    437 ;	-----------------------------------------
      00829A                        438 _gpio_setup:
                                    439 ;	src/main.c: 46: PA_DDR |= (1 << STATUS_LED_PIN); // Config open drain
      00829A 72 16 50 02      [ 1]  440 	bset	20482, #3
                                    441 ;	src/main.c: 47: PA_CR1 &= ~(1 << STATUS_LED_PIN); // Set open drain mode
      00829E 72 17 50 03      [ 1]  442 	bres	20483, #3
                                    443 ;	src/main.c: 48: PA_CR2 |= (0 << STATUS_LED_PIN); // Disable fast mode
      0082A2 C6 50 04         [ 1]  444 	ld	a, 0x5004
      0082A5 C7 50 04         [ 1]  445 	ld	0x5004, a
                                    446 ;	src/main.c: 51: PD_DDR |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Enable output
      0082A8 C6 50 11         [ 1]  447 	ld	a, 0x5011
      0082AB AA 1C            [ 1]  448 	or	a, #0x1c
      0082AD C7 50 11         [ 1]  449 	ld	0x5011, a
                                    450 ;	src/main.c: 52: PD_CR1 |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Set push-pull mode
      0082B0 C6 50 12         [ 1]  451 	ld	a, 0x5012
      0082B3 AA 1C            [ 1]  452 	or	a, #0x1c
      0082B5 C7 50 12         [ 1]  453 	ld	0x5012, a
                                    454 ;	src/main.c: 53: PD_CR2 |= (0 << RED_CHAN) | (0 << GREEN_CHAN) | (0 << BLUE_CHAN); // Set fast mode
      0082B8 C6 50 13         [ 1]  455 	ld	a, 0x5013
      0082BB C7 50 13         [ 1]  456 	ld	0x5013, a
                                    457 ;	src/main.c: 57: }
      0082BE 81               [ 4]  458 	ret
                                    459 ;	src/main.c: 60: opbyte_setup()
                                    460 ;	-----------------------------------------
                                    461 ;	 function opbyte_setup
                                    462 ;	-----------------------------------------
      0082BF                        463 _opbyte_setup:
                                    464 ;	src/main.c: 69: if ((OPT2 == opt2) && (NOPT2 == ~opt2))
      0082BF C6 48 03         [ 1]  465 	ld	a, 0x4803
      0082C2 A1 02            [ 1]  466 	cp	a, #0x02
      0082C4 26 14            [ 1]  467 	jrne	00102$
      0082C6 C6 48 04         [ 1]  468 	ld	a, 0x4804
      0082C9 5F               [ 1]  469 	clrw	x
      0082CA 97               [ 1]  470 	ld	xl, a
      0082CB A3 FF FD         [ 2]  471 	cpw	x, #0xfffd
      0082CE 26 0A            [ 1]  472 	jrne	00102$
                                    473 ;	src/main.c: 71: printf("skipping\r\n");
      0082D0 4B 27            [ 1]  474 	push	#<(___str_1 + 0)
      0082D2 4B 80            [ 1]  475 	push	#((___str_1 + 0) >> 8)
      0082D4 CD 81 0D         [ 4]  476 	call	_printf
      0082D7 5B 02            [ 2]  477 	addw	sp, #2
                                    478 ;	src/main.c: 72: return;
      0082D9 81               [ 4]  479 	ret
      0082DA                        480 00102$:
                                    481 ;	src/main.c: 74: printf("Setting DUKR keys....\n\r");
      0082DA 4B 32            [ 1]  482 	push	#<(___str_2 + 0)
      0082DC 4B 80            [ 1]  483 	push	#((___str_2 + 0) >> 8)
      0082DE CD 81 0D         [ 4]  484 	call	_printf
      0082E1 5B 02            [ 2]  485 	addw	sp, #2
                                    486 ;	src/main.c: 75: FLASH_DUKR = FLASH_DUKR_KEY1;
      0082E3 35 AE 50 64      [ 1]  487 	mov	0x5064+0, #0xae
                                    488 ;	src/main.c: 76: FLASH_DUKR = FLASH_DUKR_KEY2;
      0082E7 35 56 50 64      [ 1]  489 	mov	0x5064+0, #0x56
                                    490 ;	src/main.c: 77: printf("Waiting for writes....\n\r");
      0082EB 4B 4A            [ 1]  491 	push	#<(___str_3 + 0)
      0082ED 4B 80            [ 1]  492 	push	#((___str_3 + 0) >> 8)
      0082EF CD 81 0D         [ 4]  493 	call	_printf
      0082F2 5B 02            [ 2]  494 	addw	sp, #2
                                    495 ;	src/main.c: 78: while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_DUL)));
      0082F4                        496 00104$:
      0082F4 C6 50 5F         [ 1]  497 	ld	a, 0x505f
      0082F7 A5 08            [ 1]  498 	bcp	a, #0x08
      0082F9 27 F9            [ 1]  499 	jreq	00104$
                                    500 ;	src/main.c: 80: printf("Unlocking flash....\n\r");
      0082FB 4B 63            [ 1]  501 	push	#<(___str_4 + 0)
      0082FD 4B 80            [ 1]  502 	push	#((___str_4 + 0) >> 8)
      0082FF CD 81 0D         [ 4]  503 	call	_printf
      008302 5B 02            [ 2]  504 	addw	sp, #2
                                    505 ;	src/main.c: 81: FLASH_CR2 |= (1 << FLASH_CR2_OPT);
      008304 72 1E 50 5B      [ 1]  506 	bset	20571, #7
                                    507 ;	src/main.c: 82: FLASH_NCR2 &= ~(1 << FLASH_NCR2_NOPT);
      008308 72 1F 50 5C      [ 1]  508 	bres	20572, #7
                                    509 ;	src/main.c: 88: printf("Remapping pins....\n\r");
      00830C 4B 79            [ 1]  510 	push	#<(___str_5 + 0)
      00830E 4B 80            [ 1]  511 	push	#((___str_5 + 0) >> 8)
      008310 CD 81 0D         [ 4]  512 	call	_printf
      008313 5B 02            [ 2]  513 	addw	sp, #2
                                    514 ;	src/main.c: 89: OPT2 = opt2; // Remap timer 3 from PA3 to PD2
      008315 35 02 48 03      [ 1]  515 	mov	0x4803+0, #0x02
                                    516 ;	src/main.c: 90: NOPT2 = ~opt2; // Remap timer 3 from PA3 to PD2
      008319 35 FD 48 04      [ 1]  517 	mov	0x4804+0, #0xfd
                                    518 ;	src/main.c: 91: printf("Waiting for writes....\n\r");
      00831D 4B 4A            [ 1]  519 	push	#<(___str_3 + 0)
      00831F 4B 80            [ 1]  520 	push	#((___str_3 + 0) >> 8)
      008321 CD 81 0D         [ 4]  521 	call	_printf
      008324 5B 02            [ 2]  522 	addw	sp, #2
                                    523 ;	src/main.c: 92: while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_EOP)));
      008326                        524 00107$:
      008326 C6 50 5F         [ 1]  525 	ld	a, 0x505f
      008329 A5 04            [ 1]  526 	bcp	a, #0x04
      00832B 27 F9            [ 1]  527 	jreq	00107$
                                    528 ;	src/main.c: 96: printf("Locking flash....\n\r");
      00832D 4B 8E            [ 1]  529 	push	#<(___str_6 + 0)
      00832F 4B 80            [ 1]  530 	push	#((___str_6 + 0) >> 8)
      008331 CD 81 0D         [ 4]  531 	call	_printf
      008334 5B 02            [ 2]  532 	addw	sp, #2
                                    533 ;	src/main.c: 97: FLASH_IAPSR &= ~(1 << FLASH_IAPSR_DUL);
      008336 72 17 50 5F      [ 1]  534 	bres	20575, #3
                                    535 ;	src/main.c: 98: }
      00833A 81               [ 4]  536 	ret
                                    537 ;	src/main.c: 101: main() 
                                    538 ;	-----------------------------------------
                                    539 ;	 function main
                                    540 ;	-----------------------------------------
      00833B                        541 _main:
      00833B 52 1F            [ 2]  542 	sub	sp, #31
                                    543 ;	src/main.c: 103: clock_setup();
      00833D CD 82 7D         [ 4]  544 	call	_clock_setup
                                    545 ;	src/main.c: 104: gpio_setup();
      008340 CD 82 9A         [ 4]  546 	call	_gpio_setup
                                    547 ;	src/main.c: 105: uart_setup();
      008343 CD 80 E3         [ 4]  548 	call	_uart_setup
                                    549 ;	src/main.c: 107: printf("\033[2J");
      008346 4B A2            [ 1]  550 	push	#<(___str_7 + 0)
      008348 4B 80            [ 1]  551 	push	#((___str_7 + 0) >> 8)
      00834A CD 81 0D         [ 4]  552 	call	_printf
      00834D 5B 02            [ 2]  553 	addw	sp, #2
                                    554 ;	src/main.c: 108: printf("\033[1;1H");
      00834F 4B A7            [ 1]  555 	push	#<(___str_8 + 0)
      008351 4B 80            [ 1]  556 	push	#((___str_8 + 0) >> 8)
      008353 CD 81 0D         [ 4]  557 	call	_printf
      008356 5B 02            [ 2]  558 	addw	sp, #2
                                    559 ;	src/main.c: 109: printf("Setting opbytes....\n\r");
      008358 4B AE            [ 1]  560 	push	#<(___str_9 + 0)
      00835A 4B 80            [ 1]  561 	push	#((___str_9 + 0) >> 8)
      00835C CD 81 0D         [ 4]  562 	call	_printf
      00835F 5B 02            [ 2]  563 	addw	sp, #2
                                    564 ;	src/main.c: 110: opbyte_setup();
      008361 CD 82 BF         [ 4]  565 	call	_opbyte_setup
                                    566 ;	src/main.c: 111: printf("Done!\n\r");
      008364 4B C4            [ 1]  567 	push	#<(___str_10 + 0)
      008366 4B 80            [ 1]  568 	push	#((___str_10 + 0) >> 8)
      008368 CD 81 0D         [ 4]  569 	call	_printf
      00836B 5B 02            [ 2]  570 	addw	sp, #2
                                    571 ;	src/main.c: 115: PD_ODR |= ((1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN));
      00836D C6 50 0F         [ 1]  572 	ld	a, 0x500f
      008370 AA 1C            [ 1]  573 	or	a, #0x1c
      008372 C7 50 0F         [ 1]  574 	ld	0x500f, a
                                    575 ;	src/main.c: 118: tim_setup();
      008375 CD 81 7B         [ 4]  576 	call	_tim_setup
                                    577 ;	src/main.c: 119: set_rgb(1, 1, 1);
      008378 4B 01            [ 1]  578 	push	#0x01
      00837A 4B 00            [ 1]  579 	push	#0x00
      00837C 4B 01            [ 1]  580 	push	#0x01
      00837E 4B 00            [ 1]  581 	push	#0x00
      008380 4B 01            [ 1]  582 	push	#0x01
      008382 4B 00            [ 1]  583 	push	#0x00
      008384 CD 81 E8         [ 4]  584 	call	_set_rgb
      008387 5B 06            [ 2]  585 	addw	sp, #6
                                    586 ;	src/main.c: 121: printf("\n\rStarting main loop\n\r");
      008389 4B CC            [ 1]  587 	push	#<(___str_11 + 0)
      00838B 4B 80            [ 1]  588 	push	#((___str_11 + 0) >> 8)
      00838D CD 81 0D         [ 4]  589 	call	_printf
      008390 5B 02            [ 2]  590 	addw	sp, #2
                                    591 ;	src/main.c: 140: for (ptr=0; ptr<16; ptr++)
      008392                        592 00112$:
      008392 4F               [ 1]  593 	clr	a
      008393                        594 00106$:
                                    595 ;	src/main.c: 142: uart_buf[ptr] == uart_read();
      008393 88               [ 1]  596 	push	a
      008394 CD 81 02         [ 4]  597 	call	_uart_read
      008397 84               [ 1]  598 	pop	a
                                    599 ;	src/main.c: 140: for (ptr=0; ptr<16; ptr++)
      008398 4C               [ 1]  600 	inc	a
      008399 A1 10            [ 1]  601 	cp	a, #0x10
      00839B 25 F6            [ 1]  602 	jrc	00106$
                                    603 ;	src/main.c: 155: for (ptr=0; ptr<16; ptr++)
      00839D 96               [ 1]  604 	ldw	x, sp
      00839E 5C               [ 1]  605 	incw	x
      00839F 1F 1D            [ 2]  606 	ldw	(0x1d, sp), x
      0083A1 0F 1F            [ 1]  607 	clr	(0x1f, sp)
      0083A3                        608 00108$:
                                    609 ;	src/main.c: 157: uart_write(uart_buf[ptr]);
      0083A3 5F               [ 1]  610 	clrw	x
      0083A4 7B 1F            [ 1]  611 	ld	a, (0x1f, sp)
      0083A6 97               [ 1]  612 	ld	xl, a
      0083A7 72 FB 1D         [ 2]  613 	addw	x, (0x1d, sp)
      0083AA F6               [ 1]  614 	ld	a, (x)
      0083AB 88               [ 1]  615 	push	a
      0083AC CD 80 F4         [ 4]  616 	call	_uart_write
      0083AF 84               [ 1]  617 	pop	a
                                    618 ;	src/main.c: 158: nr();
      0083B0 CD 81 50         [ 4]  619 	call	_nr
                                    620 ;	src/main.c: 155: for (ptr=0; ptr<16; ptr++)
      0083B3 0C 1F            [ 1]  621 	inc	(0x1f, sp)
      0083B5 7B 1F            [ 1]  622 	ld	a, (0x1f, sp)
      0083B7 A1 10            [ 1]  623 	cp	a, #0x10
      0083B9 25 E8            [ 1]  624 	jrc	00108$
      0083BB 20 D5            [ 2]  625 	jra	00112$
                                    626 ;	src/main.c: 208: }
      0083BD 5B 1F            [ 2]  627 	addw	sp, #31
      0083BF 81               [ 4]  628 	ret
                                    629 	.area CODE
                                    630 	.area CONST
                                    631 	.area CONST
      008024                        632 ___str_0:
      008024 0D                     633 	.db 0x0d
      008025 0A                     634 	.db 0x0a
      008026 00                     635 	.db 0x00
                                    636 	.area CODE
                                    637 	.area CONST
      008027                        638 ___str_1:
      008027 73 6B 69 70 70 69 6E   639 	.ascii "skipping"
             67
      00802F 0D                     640 	.db 0x0d
      008030 0A                     641 	.db 0x0a
      008031 00                     642 	.db 0x00
                                    643 	.area CODE
                                    644 	.area CONST
      008032                        645 ___str_2:
      008032 53 65 74 74 69 6E 67   646 	.ascii "Setting DUKR keys...."
             20 44 55 4B 52 20 6B
             65 79 73 2E 2E 2E 2E
      008047 0A                     647 	.db 0x0a
      008048 0D                     648 	.db 0x0d
      008049 00                     649 	.db 0x00
                                    650 	.area CODE
                                    651 	.area CONST
      00804A                        652 ___str_3:
      00804A 57 61 69 74 69 6E 67   653 	.ascii "Waiting for writes...."
             20 66 6F 72 20 77 72
             69 74 65 73 2E 2E 2E
             2E
      008060 0A                     654 	.db 0x0a
      008061 0D                     655 	.db 0x0d
      008062 00                     656 	.db 0x00
                                    657 	.area CODE
                                    658 	.area CONST
      008063                        659 ___str_4:
      008063 55 6E 6C 6F 63 6B 69   660 	.ascii "Unlocking flash...."
             6E 67 20 66 6C 61 73
             68 2E 2E 2E 2E
      008076 0A                     661 	.db 0x0a
      008077 0D                     662 	.db 0x0d
      008078 00                     663 	.db 0x00
                                    664 	.area CODE
                                    665 	.area CONST
      008079                        666 ___str_5:
      008079 52 65 6D 61 70 70 69   667 	.ascii "Remapping pins...."
             6E 67 20 70 69 6E 73
             2E 2E 2E 2E
      00808B 0A                     668 	.db 0x0a
      00808C 0D                     669 	.db 0x0d
      00808D 00                     670 	.db 0x00
                                    671 	.area CODE
                                    672 	.area CONST
      00808E                        673 ___str_6:
      00808E 4C 6F 63 6B 69 6E 67   674 	.ascii "Locking flash...."
             20 66 6C 61 73 68 2E
             2E 2E 2E
      00809F 0A                     675 	.db 0x0a
      0080A0 0D                     676 	.db 0x0d
      0080A1 00                     677 	.db 0x00
                                    678 	.area CODE
                                    679 	.area CONST
      0080A2                        680 ___str_7:
      0080A2 1B                     681 	.db 0x1b
      0080A3 5B 32 4A               682 	.ascii "[2J"
      0080A6 00                     683 	.db 0x00
                                    684 	.area CODE
                                    685 	.area CONST
      0080A7                        686 ___str_8:
      0080A7 1B                     687 	.db 0x1b
      0080A8 5B 31 3B 31 48         688 	.ascii "[1;1H"
      0080AD 00                     689 	.db 0x00
                                    690 	.area CODE
                                    691 	.area CONST
      0080AE                        692 ___str_9:
      0080AE 53 65 74 74 69 6E 67   693 	.ascii "Setting opbytes...."
             20 6F 70 62 79 74 65
             73 2E 2E 2E 2E
      0080C1 0A                     694 	.db 0x0a
      0080C2 0D                     695 	.db 0x0d
      0080C3 00                     696 	.db 0x00
                                    697 	.area CODE
                                    698 	.area CONST
      0080C4                        699 ___str_10:
      0080C4 44 6F 6E 65 21         700 	.ascii "Done!"
      0080C9 0A                     701 	.db 0x0a
      0080CA 0D                     702 	.db 0x0d
      0080CB 00                     703 	.db 0x00
                                    704 	.area CODE
                                    705 	.area CONST
      0080CC                        706 ___str_11:
      0080CC 0A                     707 	.db 0x0a
      0080CD 0D                     708 	.db 0x0d
      0080CE 53 74 61 72 74 69 6E   709 	.ascii "Starting main loop"
             67 20 6D 61 69 6E 20
             6C 6F 6F 70
      0080E0 0A                     710 	.db 0x0a
      0080E1 0D                     711 	.db 0x0d
      0080E2 00                     712 	.db 0x00
                                    713 	.area CODE
                                    714 	.area INITIALIZER
                                    715 	.area CABS (ABS)
