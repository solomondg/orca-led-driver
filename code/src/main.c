#ifndef __main_H
#include "main.h"
#endif

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
//#include <stdio.h>

#ifndef __pins_H
#include "pins.h"
#endif

#include "uart.h"
#include "spi.h"
#include "tim.h"
#include "mathutils.h"

void 
DELAY_MS(uint16_t __ms)
{
    while (__ms--)
    {
        //__delay_us(1000);
        __delay_cycles( __t_count(1000));
    }
}

void 
clock_setup()
{
    CLK_CSSR |= (1 << 0) | (1 << 2);

    CLK_ECKR |= (1 << 0); // enable HSE clock
    CLK_SWR = 0xB4;

    CLK_CKDIVR = 0x00;

    // Enable UART, TIM1/2, and SPI
    CLK_PCKENR1 |= (1 << 2) | (1 << 3) | (1 << 5) | (1 << 7) | (1 << 1); 
}

void 
gpio_setup()
{
    PA_DDR |= (1 << STATUS_LED_PIN); // Config open drain
    PA_CR1 &= ~(1 << STATUS_LED_PIN); // Set open drain mode
    PA_CR2 |= (0 << STATUS_LED_PIN); // Disable fast mode


    PD_DDR |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Enable output
    PD_CR1 |= (1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN); // Set push-pull mode
    PD_CR2 |= (0 << RED_CHAN) | (0 << GREEN_CHAN) | (0 << BLUE_CHAN); // Set fast mode
    //PD_DDR|=0x04;
    //PD_CR1|=0x04;
    //PD_CR2&=0XFD;
}

void
opbyte_setup()
{
    uint8_t opt2 = 0x2;
    //printi(opt2);
    //nr();
    //printi(OPT2);
    //nr();
    //printi(NOPT2);
    //nr();
    if ((OPT2 == opt2) && (NOPT2 == ~opt2))
    {
        printf("skipping\r\n");
        return;
    }
    printf("Setting DUKR keys....\n\r");
    FLASH_DUKR = FLASH_DUKR_KEY1;
    FLASH_DUKR = FLASH_DUKR_KEY2;
    printf("Waiting for writes....\n\r");
    while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_DUL)));

    printf("Unlocking flash....\n\r");
    FLASH_CR2 |= (1 << FLASH_CR2_OPT);
    FLASH_NCR2 &= ~(1 << FLASH_NCR2_NOPT);

    //OPT2 = 0;
    //NOPT2 = ~OPT2;
    //

    printf("Remapping pins....\n\r");
    OPT2 = opt2; // Remap timer 3 from PA3 to PD2
    NOPT2 = ~opt2; // Remap timer 3 from PA3 to PD2
    printf("Waiting for writes....\n\r");
    while (!(FLASH_IAPSR & (1 << FLASH_IAPSR_EOP)));

    //FLASH_CR2 = 0;
    //FLASH_NCR2 = ~FLASH_CR2;
    printf("Locking flash....\n\r");
    FLASH_IAPSR &= ~(1 << FLASH_IAPSR_DUL);
}

void 
main() 
{
    clock_setup();
    gpio_setup();
    uart_setup();
    //printf("\e[1;1H\e[2J");
    printf("\033[2J");
    printf("\033[1;1H");
    printf("Setting opbytes....\n\r");
    opbyte_setup();
    printf("Done!\n\r");

    //CLK_PCKENR1 = 0xff;

    PD_ODR |= ((1 << RED_CHAN) | (1 << GREEN_CHAN) | (1 << BLUE_CHAN));
    //DELAY_MS(1000);

    tim_setup();
    set_rgb(1, 1, 1);

    printf("\n\rStarting main loop\n\r");

    //{0000,0000,0000}
    char uart_buf[16];
    uint8_t ptr = 0;
    uint8_t ptr2 = 0;
    uint8_t startchar = '{';
    uint8_t sepchar = ',';
    uint8_t endchar = '}';

    while (1)
    {

        //ptr=0;
        //do
        //{
        //    uart_buf[0] = uart_read();
        //} while (uart_buf[0] != startchar);

        for (ptr=0; ptr<16; ptr++)
        {
            uart_buf[ptr] == uart_read();
        }

        
        char r[4];
        char g[4];
        char b[4];

        //memcpy(&(r[0]), &(uart_buf[1]), 4);
        //memcpy(&(g[0]), &(uart_buf[6]), 4);
        //memcpy(&(b[0]), &(uart_buf[11]), 4);

        //printf(uart_buf);
        for (ptr=0; ptr<16; ptr++)
        {
            uart_write(uart_buf[ptr]);
            nr();
        }
    }
/*
    while(1) {

        //hsvrgb(h++, 0xFF, 0xFF, &r, &g, &b);
        uint16_t n = 1024;

        for (uint16_t i=0; i<n; i++)
        {
            uint16_t r, g, b;
            r = adjust_vision(i);
            g = adjust_vision(i);
            b = adjust_vision(i);
            //adjust_vision(&r, &g, &b);
            set_rgb(r,g,b);
            __delay_cycles(__t_count(1));
            //_uitoa(r, tim_buf, 10);
            //printf(tim_buf);
            //printf("\n\r");
        }
        PA_ODR ^= (1 << STATUS_LED_PIN);
        for (uint16_t i=n; i>0; i--)
        {
            uint16_t r, g, b;
            r = adjust_vision(i);
            g = adjust_vision(i);
            b = adjust_vision(i);
            //adjust_vision(&r, &g, &b);
            set_rgb(r,g,b);
            __delay_cycles(__t_count(1));
            //_uitoa(r, tim_buf, 10);
            //printf(tim_buf);
            //printf("\n\r");
        }
        PA_ODR ^= (1 << STATUS_LED_PIN);
        
        uint8_t ret = uart_read();
        printf(&ret);
        nr();

        //uart_write();
        //uint16_t tmpcntr = ((uint16_t) TIM2_CNTRH << 8) | ((uint16_t) TIM2_CNTRL);
        //printf("\r"); for(uint8_t i=0; i<tmpcntr>>2; i++) {printf("|")}
        //printf(tim_buf);
        //printf("\n\r");
        //free(test);
    }
    */
}

