#include "typedefs.h"
#include <stdint.h>
#include "pins.h"
#include "main.h"

// We want ~1KHz LED switching with 8 bit control fidelity
// Fio = Fclk/(2*prescaler*(1+ARR))
// If we have a 16MHz clock, we can prescale it using 2^2
// Then count to 4095 to get ~976Hz, which is pretty close enough


void
tim_setup()
{
    //__asm__("rim");

    TIM2_PSCR = 2; // Prescale by 4

    // To configure autoreload, we need to send the upper byte of 4095 to ARRH and lower to ARRL
    TIM2_ARRH = 4095 >> 0x8;
    TIM2_ARRL = 4095 & 0xFF;

    //             PWM Mode 2   Enable preload
    TIM2_CCMR1 |= (0b111 << 4) | (0 << 3);
    //            Enable output
    TIM2_CCMR1 &= ~(0b11 << 0);
    TIM2_CCMR2 |= (0b111 << 4) | (0 << 3);
    TIM2_CCMR2 &= ~(0b11 << 0);
    TIM2_CCMR3 |= (0b111 << 4) | (0 << 3);
    TIM2_CCMR3 &= ~(0b11 << 0);
    

    TIM2_CCER1 |= (0b11 << 4) | (0b11 << 0);
    TIM2_CCER2 |= (0b11 << 0);
   
    TIM2_CR1 |= 0b1;
    //TIM2_IER |= 0b1110;
    TIM2_IER |= 0;
    TIM2_IER |= 1 << 6;
    

}

/*
static uint8_t light_map[256] = {
        0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,2,
        2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,
        4,4,4,4,4,4,5,5,5,5,5,6,6,6,6,6,
        7,7,7,7,8,8,8,8,9,9,9,10,10,10,10,11,
        11,11,12,12,12,13,13,13,14,14,15,15,15,16,16,17,
        17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,
        25,25,26,26,27,28,28,29,29,30,31,31,32,32,33,34,
        34,35,36,37,37,38,39,39,40,41,42,43,43,44,45,46,
        47,47,48,49,50,51,52,53,54,54,55,56,57,58,59,60,
        61,62,63,64,65,66,67,68,70,71,72,73,74,75,76,77,
        79,80,81,82,83,85,86,87,88,90,91,92,94,95,96,98,
        99,100,102,103,105,106,108,109,110,112,113,115,116,118,120,121,
        123,124,126,128,129,131,132,134,136,138,139,141,143,145,146,148,
        150,152,154,155,157,159,161,163,165,167,169,171,173,175,177,179,
        181,183,185,187,189,191,193,196,198,200,202,204,207,209,211,214,
        216,218,220,223,225,228,230,232,235,237,240,242,245,247,250,252
};
*/


static inline uint16_t
adjust_vision(uint16_t v)
{
    return (uint16_t) ((((uint32_t) v) * ((uint32_t) v)) >> 12);
}


void
set_rgb(uint16_t r, uint16_t g, uint16_t b)
{
    TIM2_CCR1H = (r) >> 0x8;
    TIM2_CCR1L = (r) & 0xFF;
    TIM2_CCR2H = (g) >> 0x8;
    TIM2_CCR2L = (g) & 0xFF;
    TIM2_CCR3H = (b) >> 0x8;
    TIM2_CCR3L = (b) & 0xFF;
}

static inline uint16_t
get_counter()
{
    return (((uint16_t) TIM2_CNTRH) << 8) | ((uint16_t) TIM2_CNTRL);
}
