#ifndef __main_H
#define __main_H
#endif

#include <stdint.h>

#ifndef __typedefs_H
#include "typedefs.h"
#endif


#define F_CPU 16000000UL

#define RED_CHAN 4
#define GREEN_CHAN 3
#define BLUE_CHAN 2
#define STATUS_LED_PIN 3

#define __t_count(x) (( F_CPU * x / 1000000UL )-5)/5

#define __delay_cycles(ms) { \
    __asm__("nop\n nop\n"); \
    uint32_t __ticks = ms;  \
    do {                    \
        __ticks--;          \
    } while (__ticks);      \
    __asm__("nop\n");       \
}

#define __delay_us (__us) {             \
    __delay_cycles( __t_count(__us));   \
}
