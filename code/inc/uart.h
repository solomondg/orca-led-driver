#ifndef __uart_H
#define __uart_H
#endif
#ifndef __typedefs_H
#include "typedefs.h"
#endif
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "pins.h"

#define UART_DIV 0x8B // 16 MHz / 9600 baud
#define BRR2UPPER UART_DIV >> 0xC
#define BRR2LOWER UART_DIV & 0xF
#define BRR2 (BRR2UPPER << 0x4) | BRR2LOWER
#define BRR1 (UART_DIV >> 0x4) & 0xFF

#define UART_ENABLE_RX 3
#define UART_ENABLE_TX 2

#define UART_TXE    7
#define UART_TC     6
#define UART_RXNE   5

void
uart_setup()                                       
{                                                            
    UART_BRR2 = BRR2;                                         
    UART_BRR1 = BRR1;                                          
    UART_CR2 |= (1 << UART_ENABLE_RX) | (1 << UART_ENABLE_TX); 
    //UART_CR1 |= 
}

void
uart_write(uint8_t data)                 
{                                        
    UART_DR = data;                      
    while (!(UART_SR & (1 << UART_TC))); 
}                                        

uint8_t 
uart_read() {
    while (!(UART_SR & (1 << UART_RXNE)));
    return UART_DR;
}

void 
printf(const char *str)
{                
    uint8_t __len = strlen(str);
    for (uint8_t __i=0; __i<__len; __i++ )
        uart_write(str[__i]);     
}

void
printi(uint16_t i)
{
    char buf[16];
    _uitoa(i, buf, 10);
    printf(buf);
}

void
nr()
{
    printf("\r\n");
}
