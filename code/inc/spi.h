#ifndef __spi_H
#define __spi_H
#endif

#include "typedefs.h"
#include "pins.h"

#define SPE         6
#define BR0         3
#define MSTR        2
#define SSM         1
#define SSI         0
#define BSY         7
#define TXE         1
#define RXNE        0

void 
spi_init()
{
    // Note, use SPI_MODE0 and MSBFIRST if driving from an arduino
    //
    // init spi slave at 1MHz
    SPI_CR1 |= 1 << SPE; // Enable SPI
    SPI_CR1 |= 0b011 << BR0; // Set 1MHz mode
    SPI_CR1 &= ~(0b1<<MSTR); // Slave mode
    SPI_CR1 &= ~(0b11); // Idle polarity 0, clock phase first
    SPI_CR2 |= (1 << SSM) | (0 << SSI);
    SPI_CR2 |= (1 << 2); // Set RXOnly (disable output, makes programming easier)
}
