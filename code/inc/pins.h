#ifndef __pins_H
#define __pins_H
#endif

// Converts hardware register map address
#define _SFR_(mem_addr)      (*(volatile uint8_t *)(0x5000 + (mem_addr)))
#define _OPBYTE_(mem_addr)      (*(volatile uint8_t *)(0x4800 + (mem_addr)))
#define _MEM_(mem_addr)         (*(volatile uint8_t *)(mem_addr))
#define _EEPROM_(mem_addr)         (*(volatile uint8_t *)(0x4000 + (mem_addr)))

// GPIO A
#define PA_ODR _SFR_(0x00)
#define PA_IDR _SFR_(0x01)
#define PA_DDR _SFR_(0x02)
#define PA_CR1 _SFR_(0x03)
#define PA_CR2 _SFR_(0x04)

// GPIO B
#define PB_ODR _SFR_(0x05)
#define PB_IDR _SFR_(0x06)
#define PB_DDR _SFR_(0x07)
#define PB_CR1 _SFR_(0x08)
#define PB_CR2 _SFR_(0x09)

// GPIO C
#define PC_ODR _SFR_(0x0A)
#define PC_IDR _SFR_(0x0B)
#define PC_DDR _SFR_(0x0C)
#define PC_CR1 _SFR_(0x0D)
#define PC_CR2 _SFR_(0x0E)

// GPIO D
#define PD_ODR _SFR_(0x0F)
#define PD_IDR _SFR_(0x10)
#define PD_DDR _SFR_(0x11)
#define PD_CR1 _SFR_(0x12)
#define PD_CR2 _SFR_(0x13)

// Clock control
#define CLK_ICKR _SFR_(0xC0)
#define CLK_ECKR _SFR_(0xC1)
#define CLK_CMSR     _SFR_(0xC3)
#define CLK_SWR      _SFR_(0xC4)
#define CLK_SWCR     _SFR_(0xC5)
#define CLK_CKDIVR   _SFR_(0xC6)
#define CLK_PCKENR1  _SFR_(0xC7)
#define CLK_CSSR     _SFR_(0xC8)
#define CLK_CCOR     _SFR_(0xC9)
#define CLK_PCKENR2  _SFR_(0xCA)
#define CLK_HSITRIMR _SFR_(0xCC)
#define CLK_SWIMCCR   _SFR_(0xCD)

// SPI control
#define SPI_CR1 _SFR_(0x200)
#define SPI_CR2 _SFR_(0x201)
#define SPI_ICR _SFR_(0x202)
#define SPI_SR _SFR_(0x203)
#define SPI_DR _SFR_(0x204)
#define SPI_CRCPR _SFR_(0x205)
#define SPI_RXCRCR _SFR_(0x206)
#define SPI_TXCRCR _SFR_(0x207)

// UART control
#define UART_SR _SFR_(0x230)
#define UART_DR _SFR_(0x231)
#define UART_BRR1 _SFR_(0x232)
#define UART_BRR2 _SFR_(0x233)
#define UART_CR1 _SFR_(0x234)
#define UART_CR2 _SFR_(0x235)
#define UART_CR3 _SFR_(0x236)
#define UART_CR4 _SFR_(0x237)
#define UART_CR5 _SFR_(0x238)
#define UART_GTR _SFR_(0x239)
#define UART_PSCR _SFR_(0x23A)

// TIM2 control
#define TIM2_CR1   _SFR_(0x300)
#define TIM2_IER   _SFR_(0x303)
#define TIM2_SR1   _SFR_(0x304)
#define TIM2_SR2   _SFR_(0x305)
#define TIM2_EGR   _SFR_(0x306)
#define TIM2_CCMR1 _SFR_(0x307)
#define TIM2_CCMR2 _SFR_(0x308)
#define TIM2_CCMR3 _SFR_(0x309)
#define TIM2_CCER1 _SFR_(0x30A)
#define TIM2_CCER2 _SFR_(0x30B)
#define TIM2_CNTRH _SFR_(0x30C)
#define TIM2_CNTRL _SFR_(0x30D)
#define TIM2_PSCR  _SFR_(0x30E)
#define TIM2_ARRH  _SFR_(0x30F)
#define TIM2_ARRL  _SFR_(0x310)
#define TIM2_CCR1H _SFR_(0x311)
#define TIM2_CCR1L _SFR_(0x312)
#define TIM2_CCR2H _SFR_(0x313)
#define TIM2_CCR2L _SFR_(0x314)
#define TIM2_CCR3H _SFR_(0x315)
#define TIM2_CCR3L _SFR_(0x316)

// Option bytes
#define OPT0 _OPBYTE_(0x0)
#define OPT1 _OPBYTE_(0x1)
#define NOPT1 _OPBYTE_(0x2)
#define OPT2 _OPBYTE_(0x3)
#define NOPT2 _OPBYTE_(0x4)
#define OPT3 _OPBYTE_(0x5)
#define NOPT3 _OPBYTE_(0x6)
#define OPT4 _OPBYTE_(0x7)
#define NOPT4 _OPBYTE_(0x8)
#define OPT5 _OPBYTE_(0x9)
#define NOPT5 _OPBYTE_(0xA)

// Flash
#define FLASH_CR2 _MEM_(0x505B)
#define FLASH_NCR2 _MEM_(0x505C)
#define FLASH_IAPSR _MEM_(0x505F)
#define FLASH_DUKR _MEM_(0x5064)

//FLASH_DUKR
#define FLASH_DUKR_KEY1 0xAE
#define FLASH_DUKR_KEY2 0x56
//FLASH_IAPSR 
#define FLASH_IAPSR_DUL 3
#define FLASH_IAPSR_EOP 2
//FLASH_CR2
#define FLASH_CR2_OPT 7
#define FLASH_NCR2_NOPT 7

